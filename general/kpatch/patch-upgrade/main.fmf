summary: Kpatch-patch upgrade test
description: |
    Verify the livepatch atomic replace.

    Atomic replace allows a new livepatch to completely
    replace older patch(es).  This is interesting because it allows us to
    maintain sources for only a single cumulative patch.  e.g

    Patch 1 - updates functions A, B, C
    Patch 2 - updates functions A,  , C, D

    with atomic replace, when loading Patch 2, the end result will update
    functions A, C, and D.  Function B will effectively be reverted.
    (Normal livepatch semantics would overlay Patch 1 and 2 such that A, B,
    C, and D would be modified).
contact: Yulia Kopkova <ykopkova@redhat.com>
test: bash ./runtest.sh
framework: beakerlib
component:
  - kernel
require:
  - wget
  - git
  - gcc
  - make
  - autoconf
  - tar
  - bzip2
  - type: file
    pattern:
      - /general/kpatch/include/lib.sh
recommend:
  - kpatch
  - nmap-ncat
duration: 240m
extra-summary: /kernel/general/kpatch/patch-upgrade
extra-task: /kernel/general/kpatch/patch-upgrade
